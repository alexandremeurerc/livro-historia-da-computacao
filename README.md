# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores]()
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Primeira Geração]()
1. [Computação Móvel]()
1. [Futuro]()




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168999/avatar.png?width=400)  | Alexandre Meurer Castanhede | alexandremeurerc | [alexandre.230701@alunos.utfpr.edu.br](mailto:alexandre.230701@alunos.utfpr.edu.br) |
| ![](https://avatars.githubusercontent.com/u/86437080?s=400&u=9b7bd52899d0103ee587fa41c9caf4f332acdbfb&v=4) | Guilherme Valnetim De Matos | guilhermevalentim | [guilhermevalentim@alunos.utfpr.edu.br](mailto:guilhermevalentim@alunos.utfpr.edu.br) |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9220251/avatar.png?width=400)  | João Gabriel Biesdorf | joaogabrielbiesdorf | [joaogabrielbiesdorf@alunos.utfpr.edu.br](mailto:joaogabrielbiesdorf@alunos.utfpr.edu.br) |
